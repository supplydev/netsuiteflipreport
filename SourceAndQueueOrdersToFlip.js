/**
 * @NApiVersion 2.x
 * @NScriptType workflowactionscript
 */
define(['N/file', 'N/email', 'N/http', 'N/record', 'N/runtime', 'N/search', 'N/url', 'N/https', 'S/accounting', 'S/hiplog'],

    function (file, email, http, record, runtime, search, url, https, accounting, hiplog) {

        var linesToFlip = [];
        var recordObj;
        var transaction = {};
        var inventory = {}; // items{distributionCenters}
        var items = new Array();
        var sourcingItemIds = new Array();
        var shippingAddress;
        var recordId;
        var shippingCosts = {};
        var logs = new Array();
        var sourcingVersion = '1.1';
        var sourcingLogs = {};
        var salesOrder;

        /**
         * Definition of the Suitelet script trigger point.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @Since 2016.1
         */
        function onAction(scriptContext) {
            if (runtime.envType != runtime.EnvType.PRODUCTION) {
                return;
            }

            recordObj = scriptContext.newRecord;

            log.debug('TRANSACTION ID', recordObj.id);
            var slackLog = "https://hooks.slack.com/services/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";
            try {
                // Select lines that need to be evaluated to flip
                // Any quantity unfulfilled and open
                for (var i = 0; i < recordObj.getLineCount('item'); i++) {
                    var quantity = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'quantity', line: i });
                    var fulfilled = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'quantityreceived', line: i });
                    var closed = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'isclosed', line: i });
                    if (quantity > fulfilled && !closed) {
                        linesToFlip.push(i);
                    }
                }
                log.debug("PO lines to flip", recordObj.id + ': ' + linesToFlip);

                var dateTime = new Date();

                // Populate objects to send to the sourcing engine
                var performSourcing = initializeData(recordObj);

                if (!performSourcing) {
                    return;
                }

                var preSourcingShipFromValue = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_feishipfrom', line: linesToFlip[0] });
                log.debug("OLD ship from value", preSourcingShipFromValue);

                // Run sourcing -- re-populates the global variables
                // FLIPPED object values start here //
                try {
                    runSourcingEngine();
                } catch (e) {
                    log.error("error", e);
                    email.send({
                        author: 15072050,
                        recipients: 'suitesquad@hmwallace.com',
                        subject: 'Flip Sourcing Engine - ERROR calling Suitelet',
                        body: e.message + '\n\n' + 'Transaction Data: \n' + recordObj.id + ': ' + JSON.stringify(transaction) + '\n\n' + 'Item Data: \n' + JSON.stringify(items)
                    });
                    var error = {
                        message: e.message + '\n\n' + 'Transaction Data: \n' + recordObj.id + ': ' + JSON.stringify(transaction) + '\n\n' + 'Item Data: \n' + JSON.stringify(items),
                        color: 'red'
                    };
                    hiplog.log(error, slackLog);
                }

                var newShipFrom = items[0].shipFrom;
                log.debug("NEW ship from value", newShipFrom);

                // Mark transaction invalid if ship from value didn't change OR items are backordered in the new DC
                // Also notify if the ship from values per line do not match because sourceComplete = true
                // NOTE: If there is a manually sourced line (DC Network - Manual), the order will not source complete and should not be flipped.
                var invalidFlip = 0;
                for (var i = 0; i < items.length; i++) {
                    if (items[i].shipFrom != newShipFrom) {
                        invalidFlip++;
                        log.debug("Invalid Flip", recordObj.id + ": did NOT source complete - Flip DC: " + newShipFrom + " != Line Ship From: " + items[i].shipFrom);
                    }
                    if (preSourcingShipFromValue == items[i].shipFrom) {
                        invalidFlip++;
                        log.debug("Invalid Flip", recordObj.id + ": sourced to the SAME DC - " + newShipFrom + " = " + items[i].shipFrom);
                    }
                    if (items[i].quantityBackordered > 0) {
                        invalidFlip++;
                        log.debug("Invalid Flip", recordObj.id + ": line " + items[i].lineId + " backordered - " + items[i].quantityBackordered);
                    }
                }
                log.debug("invalid flip?", invalidFlip);

                try {
                    if (invalidFlip == 0) {
                        record.submitFields({
                            type: 'purchaseorder',
                            id: recordObj.id,
                            values: {
                                'custbody_ss_pendingflip': true,
                                'custbody_ss_pendingflipto': newShipFrom,
                                'custbody_ss_flipdate': null
                            }
                        });
                    }
                } catch (e) {
                    log.error("error", e);
                    email.send({
                        author: 15072050,
                        recipients: 'suitesquad@hmwallace.com',
                        subject: 'Flip ERROR Marking Order to Flip',
                        body: e.message + '\nInternal Id: ' + recordObj.id
                    });
                    var error = {
                        message: e.message + '\nInternal Id: ' + recordObj.id,
                        color: 'red'
                    };
                    hiplog.log(error, slackLog);
                }

                log.debug('logs', JSON.stringify(logs));
                record.submitFields({
                    type: 'purchaseorder',
                    id: recordObj.id,
                    values: {
                        'custbody_ss_lastflipevaluation': dateTime,
                    }
                });
            } catch (e) {
                log.error("error", e);
                email.send({
                    author: 15072050,
                    recipients: 'suitesquad@hmwallace.com',
                    subject: 'Flip Sourcing ERROR',
                    body: e.message + '\nInternal Id: ' + recordObj.id
                });
                var error = {
                    message: e.message + '\nInternal Id: ' + recordObj.id,
                    color: 'red'
                };
                hiplog.log(error, slackLog);
            }
        }

        /* SOURCING ENGINE */
        function runSourcingEngine() {
            var body = {
                transaction: JSON.stringify(transaction),
                items: JSON.stringify(items),
                sourcingItemIds: JSON.stringify(sourcingItemIds),
                shippingAddress: JSON.stringify(shippingAddress)
            };
            updateSourcingLogs('request', body);
            log.debug("runSourcingEngine Body", body);

            var newSourcingSandBox = 'https://634494-sb1.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1842&deploy=1&compid=634494_SB1&h=8a8fa2fb9259d566aa83';
            var sourcingForFlip = "https://634494.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1842&deploy=2&compid=634494&h=4041e3b7c45a38be335e";

            try{
                var response = https.post({
                    url: sourcingForFlip,
                    body: JSON.stringify(body),
                    headers: {
                        "Content-Type": 'application/json'
                    }
                }).body;
            } catch(e){
                log.error("error", e);
                email.send({
                    author: 15072050,
                    recipients: 't.beauregard@hmwallace.com',
                    subject: 'Flip Sourcing Error On POST',
                    body: e.message
                });
            }
           
            try{
                log.debug('response', response);
                response = JSON.parse(response);
                items = JSON.parse(response.items);
                transaction = JSON.parse(response.transaction);
                shippingCosts = JSON.parse(response.shippingCosts);
                logs = JSON.parse(response.logs);
    
                updateSourcingLogs('response', response);
            }catch(e){
                log.error("error", e);
                email.send({
                    author: 15072050,
                    recipients: 't.beauregard@hmwallace.com',
                    subject: 'Flip Sourcing Error On reading response',
                    body: e.message
                });
            }

            
        }
        function updateSourcingLogs(type, dataIn) {

            if (type == 'request') {

                var request = dataIn;

                sourcingLogs.requestData = request.items.split(',').join('\r\n,').split('"').join(' ') + '\r\n\r\n'
                    + request.transaction.split(',').join('\r\n,').split('"').join(' ');
            }
            else if (type == 'response') {
                var response = dataIn;

                sourcingLogs.responseData = response.items.split(',').join('\r\n,').split('"').join(' ') + '\r\n\r\n'
                    + response.transaction.split(',').join('\r\n,').split('"').join(' ');

                sourcingLogs.suiteletLogs = response.logs.split(',').join('\r\n,').split('"').join(' ');
            }

        }

        // I will set up a room to log flip reporting
        function logIt(message, name) {
            var name1 = name || 'empty';
            var message1 = message || 'empty';
            var data = {
                from: name1,
                message: message1,
                notify: true,
                color: 'red'
            };

            hiplog.putsCommandCenter(data);
        }

        /* INITIALIZE DATA */
        function initializeData() {

            if (recordObj.id == '56924439') {
                email.send({
                    author: 193371,
                    recipients: 'c.pittman@hmwallace.com',
                    body: 'initializeData',
                    subject: 'Curtis Email'
                });
            }
            initializeTransactionData();
            if (recordObj.id == '56924439') {
                email.send({
                    author: 193371,
                    recipients: 'c.pittman@hmwallace.com',
                    body: JSON.stringify(transaction),
                    subject: 'Curtis Email'
                });
            }
            initializeItems();
            if (recordObj.id == '56924439') {
                email.send({
                    author: 193371,
                    recipients: 'c.pittman@hmwallace.com',
                    body: JSON.stringify(items),
                    subject: 'Curtis Email'
                });
            }
            //if no items were initialized return out of script
            if (items.length == 0) {
                return;
            }
            cleanAddress();
            return true;
        }

        /* TRANSACTION */
        function initializeTransactionData() {
            // Some body fields are only available on the SO
            salesOrder = record.load({
                type: 'salesorder',
                id: recordObj.getValue({ fieldId: 'createdfrom' })
            });

            transaction.departmentId = recordObj.getValue({ fieldId: 'department' });
            transaction.sameDayShipping = salesOrder.getValue({ fieldId: 'custbody7' }); //FROM SO
            transaction.customerId = recordObj.getValue({ fieldId: 'entity' });
            transaction.transactionType = recordObj.type;
            transaction.shipVia = recordObj.getValue({ fieldId: 'shipmethod' });
            transaction.sourceComplete = true; //all flipped orders should source complete
            transaction.shipComplete = true;
            transaction.fulfillComplete = recordObj.getValue({ fieldId: 'custbody_ss_fulfillcomplete' });
            transaction.acceptBackOrder = recordObj.getValue({ fieldId: 'custbody_ss_acceptbackorder' });
            transaction.requestedShipFrom = salesOrder.getValue({ fieldId: 'custbody_ss_requestedshipfrom' }); //FROM SO
            transaction.billShippingTo3rdParty = salesOrder.getValue({ fieldId: 'custbody_ozlink_bill_shipping_to_3rd' }); //FROM SO
            transaction.estimateShipping = salesOrder.getValue({ fieldId: 'custbody_ss_estimateshipping' }); //FROM SO
            transaction.orderType = recordObj.getValue({ fieldId : 'custbody40' });
            transaction.momentsNotice = recordObj.getValue({ fieldId : 'custbody_ss_momentsnotice' });
            transaction.splitLineSourcing = recordObj.getValue({ fieldId : 'custbody_ss_splitlinesourcing' });
            transaction.id = recordObj.id;
        }

        /* ITEMS */
        function initializeItems() {

            var itemsToSearch = [];

            // Only get data on lines to flip
            for (var i = 0; i < linesToFlip.length; i++) {
                var lineId = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'line', line: linesToFlip[i] });
                var itemId = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'item', line: linesToFlip[i] });
                var quantity = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'quantity', line: linesToFlip[i] });
                var sourcing = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_sourcing', line: linesToFlip[i] });
                var manufacturer = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_manufacturer_name', line: linesToFlip[i] });
                var isClosed = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'isclosed', line: linesToFlip[i] });
                var isOpen = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'isopen', line: linesToFlip[i] });
                var itemType = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'itemtype', line: linesToFlip[i] });
                var hasPO = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'createpo', line: linesToFlip[i] });
                var poCre = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'createpo', line: linesToFlip[i] });
                var amount = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'amount', line: linesToFlip[i] });
                var rate = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'rate', line: linesToFlip[i] });
                var reqShipDate = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_reqshipdate', line: linesToFlip[i] });
                var costEstimateRate = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'costestimaterate', line: linesToFlip[i] });
                var remainder = Number(amount % quantity) / 100;
                var customRate = Number(accounting.toFixed(Number(amount / quantity),2));
                var sourcingMethod = recordObj.getSublistValue({ sublistId : 'item', fieldId : 'custcol_ss_sourcingmethod', line : linesToFlip[i] });
                var sourcingErrorMessage = '';
                var precisionDelivery = recordObj.getSublistValue({ sublistId : 'item', fieldId : 'custcol_ss_precisiondelivery', line : linesToFlip[i] });
                var isDropShipOrderLine = recordObj.getSublistValue({ sublistId : 'item', fieldId : 'isdropshiporderline', line : linesToFlip[i]});

                itemsToSearch.push(itemId);

                // shipFrom and shipMethod are set in the suitelet?
                var shipFrom = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_feishipfrom', line: linesToFlip[i] });
                var shipMethod = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_shipmethod', line: linesToFlip[i] });

                var unapprovedKitCustomers = ['783718', '22182', '165053', '146690', '147999', '175992'];

                if (unapprovedKitCustomers.indexOf(transaction.customerId) != -1 && itemType == 'Kit' & isClosed == false) {
                    items = [];
                    Record('No Sourcing because EDI Kit Order');
                    return;
                }
                /*
                CUST40109925 Amazon.com Fulfillment Services - 2780838
                CUST75700647 Valencia Pipe Company - 7435226
                CUST40370696 RH - 3556486
                CUST2034821552 Houzz - 8247246 - Released
                CUST40031367 Liberty Procurement Co. Inc. - 783718
                CUST2034848951 Jet.com - 11614510 - Released
                CUST40078683 Hayneedle, Inc. - 1617835 Released
                Ferguson Distribution Centers 13809376
                 */

                var unapprovedCustomers = ['7435226', '3556486', '13809376'];
                if (unapprovedCustomers.indexOf(transaction.customerId) != -1) {
                    items = [];

                    //updateRecord('No Sourcing because Customer is not Released');
                    return;
                }

                //if(sourcing == 2){// Sourcing 2 = DC Network - Manual
                //	shipFrom = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_feishipfrom', line: linesToFlip[i] });
                //	shipMethod = recordObj.getSublistValue({ sublistId: 'item', fieldId: 'custcol_ss_shipmethod', line: linesToFlip[i] });	
                //}    		

                if (isOpen != 'F' && itemType == 'InvtPart') {//} && !hasPO ){ // Will definitely have a PO
                    var itemObj = {
                        itemId: itemId,
                        quantity: quantity,
                        shipMethod: shipMethod,
                        lineId: lineId,
                        sourcing: sourcing,
                        shipFrom: shipFrom,
                        index: linesToFlip[i],
                        hasPO: 'DropShip', //field not available on PO and does not seem to be used in current sourcing logic
                        isOpen: isOpen,
                        isClosed: isClosed,
                        quantityAvailable: 0, //field populated with item search results
                        isHazmat: '',  //field populated with item search results
        				amount: amount,
        				rate: rate,
        				reqShipDate: reqShipDate,
        				newLine: false,
        				quantityBackordered: 0, //always start sourcing engine with quantity backordered = 0
        				originalQuantity: quantity,
        				customRate: customRate,
        				remainder: remainder,
						costEstimateRate: costEstimateRate,
						sourcingMethod : sourcingMethod,
						precisionDelivery : precisionDelivery,
						sourcingErrorMessage : sourcingErrorMessage

                    };
                    items.push(itemObj);
                    sourcingItemIds.push(itemObj.itemId);
                }
            }
            if (!items) {
                return;
            }

            //log.debug("itemsToSearch", itemsToSearch);

            //Fill in item fields that are not available on the PO lines (quantityAvailable & isHazmat)
            var itemSearchObj = search.create({
                type: "item",
                filters:
                    [
                        ["internalid", "anyof", itemsToSearch]
                    ],
                columns:
                    [
                        search.createColumn({ name: "internalid", label: "Internal ID" }),
                        search.createColumn({ name: "custitemmanufacturer", label: "Manufacturer of Item" }),
                        search.createColumn({ name: "custitem_ss_hazmatitem", label: "Hazmat Item" }),
                        search.createColumn({ name: "custitem_ss_feiqtyavailable", label: "FEI - Quantity Available" })
                    ]
            });
            var searchResultCount = itemSearchObj.runPaged().count;
            //log.debug("itemSearchObj result count",searchResultCount);
            itemSearchObj.run().each(function (result) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].itemId == result.getValue({ name: 'internalid' })) {
                        items[i].quantityAvailable = result.getValue({ name: 'custitem_ss_feiqtyavailable' });
                        items[i].isHazmat = result.getValue({ name: 'custitem_ss_hazmatitem' });
                    }
                }
                return true;
            });
        }

        /* SHIPPING ADDRESS */
        function cleanAddress() {

            var shipSubrec = recordObj.getSubrecord({
                fieldId: 'shippingaddress'
            });

            var addressLine1 = shipSubrec.getValue({
                fieldId: 'addr1'
            });
            var addressLine2 = shipSubrec.getValue({
                fieldId: 'addr2'
            });
            var city = shipSubrec.getValue({
                fieldId: 'city'
            });
            var state = shipSubrec.getValue({
                fieldId: 'state'
            });

            var zip = shipSubrec.getValue({
                fieldId: 'zip'
            });
            var splitZip = zip.split('-');
            zip = splitZip[0].substring(0, 5);

            shippingAddress = {
                addressLine1: addressLine1,
                addressLine2: addressLine2,
                city: city,
                state: state,
                zip: zip
            };
        }

        return {
            onAction: onAction
        };

    });
