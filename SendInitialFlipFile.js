/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/email', 'N/record', 'S/hiplog', 'N/search', 'N/runtime'],

    function (file, email, record, hiplog, search, runtime) {

        /**
         * Definition of the Scheduled script trigger point.
         *
         * @param {Object} scriptContext
         * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
         * @Since 2015.2
         */
        function execute(scriptContext) {
            try {
                log.debug("Starting Script", true);
                if (runtime.envType != runtime.EnvType.PRODUCTION) {
                    return;
                }
                var slackLog = "https://hooks.slack.com/services/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";
                var recipients = [
                    "Kristin.Sgambelluri@Ferguson.com",
                    "faith.gaines@ferguson.com",
                  	"Kelsey.lister@ferguson.com"
                ];
                var date = new Date();
                var dateTime = (date.getMonth() + 1) + '-' +
                    date.getDate() + '-' +
                    date.getFullYear() + ' ' +
                    (date.getHours() + 3) + ':' + ('0' + date.getMinutes()).split(-2);
                var fileDate = (date.getMonth() + 1) + "" + date.getDate() + "" + date.getFullYear() + "" + (date.getHours() + 3) + "" + date.getMinutes();
                var body = "Attached is the Flip Report for " + dateTime + ".  " +
                    "Please send your final report after working these orders to flipreport@hmwallace.com.  " +
                    "If you have any questions or concerns please contact Jessica Brown at j.brown@hmwallace.com. " +
                    "\n\nThank you,\nNetSuite Development Team";

                var poSearchObj = search.create({
                    type: "transaction",
                    filters:
                        [
                            ["type", "anyof", "PurchOrd"],
                            "AND",
                            ["custbody_ss_pendingflip", "is", "T"],
                            "AND",
                            ["mainline", "is", "T"]
                        ],
                    columns:
                        [
                            search.createColumn({ name: "internalid", label: "Internal ID" }),
                            search.createColumn({ name: "custbody_ss_feipoconfirmationnumber", label: "FEI PO Confirmation" }),
                            search.createColumn({ name: "custbody_ss_pendingflipto", label: "Flip To DC" }),
                        ]
                });

                try {
                    flipFile = file.create({
                        name: 'Flip Report File.csv',
                        fileType: file.Type.CSV,
                        contents: 'DateTime, PO Internal Id, FEI PO Confirmation Number,DC To Flip,Flipped Yes/No,Flip Notes\n',
                        folder: 8316540
                    });
                } catch (e) {
                    email.send({
                        author: 15072050,
                        recipients: 't.beauregard@hmwallace.com',
                        subject: 'Flip File Error',
                        body: 'Error creating or updating the flip report file: ' + e.message
                    });
                    var error = {
                        message: 'Error creating or updating the flip report file: ' + e.message,
                        color: 'red'
                    };
                    hiplog.log(error, slackLog);
                }

                // Pull all orders pending flip
                var lineCount = 0;
                var flip = 1;
                poSearchObj.run().each(function (result) {
                    var poFulfilledSearchObj = search.create({
                        type: "purchaseorder",
                        filters:
                            [
                                ["type", "anyof", "PurchOrd"],
                                "AND",
                                ["applyinglinktype", "anyof", "ShipRcpt"],
                                "AND",
                                ["internalidnumber", "equalto", result.getValue({ name: 'internalid' })]
                            ],
                        columns:
                            [
                                search.createColumn({
                                    name: "internalid",
                                    summary: "GROUP",
                                    label: "Internal ID"
                                }),
                                search.createColumn({
                                    name: "custbody_ss_feifulfillmentid",
                                    join: "applyingTransaction",
                                    summary: "GROUP",
                                    label: "FEI - Item Fulfillment Id"
                                }),
                                search.createColumn({
                                    name: "statusref",
                                    summary: "GROUP",
                                    label: "Status"
                                })
                            ]
                    });
                    var quantityCommittedSearchObj = search.create({
                        type: "purchaseorder",
                        filters:
                            [
                                ["type", "anyof", "PurchOrd"],
                                "AND",
                                ["internalidnumber", "equalto", result.getValue({ name: 'internalid' })]
                            ],
                        columns:
                            [
                                search.createColumn({
                                    name: "internalid",
                                    summary: "GROUP",
                                    label: "Internal ID"
                                }),
                                search.createColumn({
                                    name: "formulanumeric",
                                    summary: "SUM",
                                    formula: "case when {quantity} > coalesce({custcol_ss_feiinitialqtycommitted},0) then 1 else 0 end ",
                                    label: "Formula (Numeric)"
                                })
                            ]
                    });

                    var po = poFulfilledSearchObj.run().getRange(0, 1);
                    var poStatus = null;

                    var quantityCommitted = quantityCommittedSearchObj.run().getRange(0, 1);
                    var quantity = 1;

                    try {
                        poStatus = po[0].getValue(poFulfilledSearchObj.columns[2]);
                    } catch (e) {
                        // We don't need to log this at all.  If this throws an error then this is the first iteration of the order.
                    }
                    try {
                        quantity = quantityCommitted[0].getValue(quantityCommittedSearchObj.columns[1]);
                        log.debug("if this is 0 then do not flip", quantity);
                    } catch (e) {
                        log.debug("no qty result");
                        // We don't need to log this at all.  If this throws an error then the order should flip.
                    }


                    // If the order have been fulfilled since it was marked to flip, we should not send it on the report
                    if (poStatus == "PurchOrd:F" || poStatus == "PurchOrd:G" || poStatus == "PurchOrd:H" || quantity == 0) {
                        record.submitFields({
                            type: 'purchaseorder',
                            id: result.getValue({ name: 'internalid' }),
                            values: {
                                'custbody_ss_pendingflip': false,
                                'custbody_ss_pendingflipto': null
                            }
                        });
                    } else {
                        var iterationCount = poFulfilledSearchObj.runPaged().count;

                        // Convert the DC id to FEI standard
                        var newShipFrom = result.getValue({ name: 'custbody_ss_pendingflipto' })
                        var flipDC = '';
                        if (newShipFrom == 1) {
                            flipDC = '423';
                        } else if (newShipFrom == 2) {
                            flipDC = '474';
                        } else if (newShipFrom == 3) {
                            flipDC = '533';
                        } else if (newShipFrom == 4) {
                            flipDC = '625';
                        } else if (newShipFrom == 5) {
                            flipDC = '761';
                        } else if (newShipFrom == 6) {
                            flipDC = '796';
                        } else if (newShipFrom == 7) {
                            flipDC = '321';
                        } else if (newShipFrom == 8) {
                            flipDC = '986';
                        } else if (newShipFrom == 9) {
                            flipDC = '2911';
                        } else if (newShipFrom == 10) {
                            flipDC = '2920';
                        } else if (newShipFrom == 2915) {
                            flipDC = '688';
                        }

                        // Get iteration for FEI PO Confirmation Number (for Trilogie)
                        var iteration = '';
                        if (iterationCount > 0) {
                            iteration = result.getValue({ name: 'custbody_ss_feipoconfirmationnumber' }) + '-' + iterationCount;
                        } else {
                            iteration = "=\"" + result.getValue({ name: 'custbody_ss_feipoconfirmationnumber' }) + "\"";
                        }

                        // Append order information to the file
                        flipFile.appendLine({
                            value: dateTime + ',' +
                                result.getValue({ name: 'internalid' }) + ',' +
                                iteration + ',' +
                                flipDC + ',' +
                                ','
                        });

                        lineCount++;
                    }

                    return true;
                });

                log.debug("Lines to Send", lineCount);

                // If there are orders to flip then send the file to all interested parties
                if (lineCount > 0) {
                    email.send({
                        author: 15072050,
                        recipients: recipients,
                        replyTo: "FlipReporter@hmwallace.com",
                        subject: "Flip Report " + dateTime,
                        body: body,
                        attachments: [flipFile]
                    });
                    hiplog.log(dateTime + " -- " + lineCount + " lines sent in the flip file.", slackLog);
                    log.debug("Logging Line Count", true);
                }
                else {	// If there are no orders to flip then send an email stating so
                    email.send({
                        author: 15072050,
                        recipients: recipients,
                        replyTo: "FlipReporter@hmwallace.com",
                        subject: "Flip Report " + dateTime,
                        body: "There are no orders to flip at this time.  Please contact j.brown@hmwallace.com with any questions or concerns. \n\nThanks\nNetSuite Development Team"
                    });
                    hiplog.log(dateTime + " -- " + "No orders to flip at this time", slackLog);
                    log.debug("Logging No Lines to Send", true);
                }

                // Always archive the file
                flipFile.name = 'FlipReport' + fileDate + '.csv';
                flipFile.folder = 8335364;
                flipFile.save();
                hiplog.log(dateTime + " -- " + "Flip file archived as " + flipFile.name, slackLog);

            } catch (e) {
                log.debug("ERROR", e);
                email.send({
                    author: 15072050,
                    recipients: 'suitesquad@hmwallace.com',
                    subject: 'Flip File Error Sending File',
                    body: e.message
                });
                var error = {
                    message: e.message,
                    color: 'red'
                };
                hiplog.log(error, slackLog);
            }

        }

        return {
            execute: execute
        };

    });
